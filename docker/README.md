Overview

Resources to manage Baxter (or simulation) in VXLab (using docker on a recent Ubuntu Linux).
This resource assumes you are familiar with docker (or can become familiar using the docker documentation).

To install docker on a recent ubuntu, run

./dockerinstall-linux

To create a docker image, run

./BUILD

To create a docker container, run

./new-baxter-container

When in RMIT, the vxlab apt-cacher-ng service acts as a local mirror for Ubuntu packages:

sudo cp /etc/apt/apt.conf.d/02proxy

(Note: this will need to be removed when not in an RMIT network.)

Reference

# docker page for Baxter
http://teslacore.blogspot.com.au/2016/07/docker-for-our-ros-robotic-overlords.html# docker page for Baxter

# DataSpeed inc - Mobility Base
https://bitbucket.org/DataspeedInc/mobility_base_simulator
